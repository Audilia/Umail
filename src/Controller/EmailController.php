<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Email;
use App\Form\EmailType;
use App\Repository\EmailRepository;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EmailController extends Controller
{
    /**
     * @Route("/", name="email")
     */
    public function index()
    {
        $email = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findAll();

        return $this->render('email/index.html.twig', [
            'controller_name' => 'EmailController',
            'email' => $email,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_id")
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $email = $em
            ->getRepository(Email::class)
            ->find($id);

        $em->remove($email);
        $em->flush();

        return $this->redirect('/');
    }

    /**
     * @Route("/new", name="new_email")
     */
    public function new(Request $request) {
        $email = new Email();
        $email->setDate(new \DateTime());

        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();
        $array_category = [];

        foreach ($category as $category) {
            $array_category[$category->getName()] = $category;
        }

        $form = $this->createForm(EmailType::class, $email)
            ->add('category', ChoiceType::class, ['label' => 'Select a category', 'choices' => $array_category])
            ->add('save', SubmitType::class, array('label' => 'Send the email'));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $email = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();

            return $this->redirect('/');
        }

        return $this->render('email/send_email.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="show_id")
     */
    public function show($id) {
        $id = $this->getDoctrine()
            ->getRepository(Email::class)
            ->find($id);

        return $this->render('email/show.html.twig', [
            'email'=> $id,
        ]);
    }


}
