<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index() {

        $contact = $this->getDoctrine()
            ->getRepository(Contact::class)
            ->findAll();

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'contact' => $contact
        ]);
    }

    /**
     * @Route("/contact/new", name="new_contact")
     */
    public function add(Request $request) {

        $contact = new Contact();
        $contact->setDate(new \DateTime());

        $form = $this->createForm(ContactType::class, $contact)
            ->add('save', SubmitType::class, array('label' => 'Create contact'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            return $this->redirect('/contact');
        }

        return $this->render('contact/new.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/contact/delete/{id}", name="delete_contact")
     */
    public function delete($id) {

        $contact = $this->getDoctrine()
            ->getManager();
        $cont = $contact
            ->getRepository(Contact::class)
            ->find($id);

        $contact->remove($cont);
        $contact->flush();

        return $this->redirect('/contact');
    }

    /**
     * @Route("/contact/{id}/edit", name="edit_contact")
     */
    public function edit(Request $request, Contact $contact): Response {

        $form = $this->createForm(ContactType::class, $contact)
        ->add('save', SubmitType::class, array('label' => 'Save the modification'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect('/contact');
        }

        return $this->render('contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }
}